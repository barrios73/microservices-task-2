﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieBookingAPI.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieBookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private readonly MovieBookingDbContext ctx;

        public BookingController(MovieBookingDbContext ctx)
        {
            this.ctx = ctx;
        }

        //POST api/Booking
        [HttpPost]
        public ActionResult PostBooking(Booking booking)
        {
            try
            {
                ctx.Bookings.Add(booking);
                ctx.SaveChanges();
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }

        }

        //PUT: api/Booking/1
        [HttpPut("{Id}")]
        public ActionResult UpdateBooking(Booking booking)
        {
            try
            {
                ctx.Update(booking);
                ctx.SaveChanges();

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }


        [HttpDelete("{Id}")]
        public ActionResult DeleteStudent(int Id)
        {
            try
            {
                Booking booking = ctx.Bookings.Where(x => x.Id == Id).First();
                ctx.Bookings.Remove(booking);
                ctx.SaveChanges();
            }
            catch (Exception ex)
            {

                return StatusCode(500);
            }


            return NoContent();
        }


        [HttpGet("{Id}")]
        public ActionResult<Booking> GetBooking(int Id)
        {
            try
            {
                Booking booking = ctx.Bookings.Where(x => x.Id == Id).First();
                return booking;
            }
            catch (Exception ex)
            {

                return StatusCode(500);
            }
        }


        [HttpGet("GetAllBookings")]
        public ActionResult<List<Booking>> Get()
        {
            return ctx.Bookings.ToList<Booking>();
        }

    }
}
