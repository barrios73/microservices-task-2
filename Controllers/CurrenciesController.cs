﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieBookingAPI.Models;
using MovieBookingAPI.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieBookingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrenciesController : ControllerBase
    {
        [HttpPost("latest/currencies/{cur}")]
        public ActionResult<ConvertedCurrency> ConvertCurrency(string cur, InputCurrency inputCurrency)
        {
            try
            {
                ConvertedCurrency amtSGD = new ConvertedCurrency();
                amtSGD.date = DateTime.Now.ToString("yyyy-MM-dd");
                amtSGD.sgd = Currency.ConvertCurrency(cur, inputCurrency);
                return amtSGD;
            }
            catch(Exception ex)
            {
                return StatusCode(500);
            }
           
        }

    }
}
