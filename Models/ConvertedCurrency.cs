﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieBookingAPI.Models
{
    public class ConvertedCurrency
    {
        public string date { get; set; }
        public decimal sgd { get; set; }
    }
}
