﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieBookingAPI.EntityModels
{
    public class Booking
    {
        public int Id { get; set; }
        public DateTime BookingDate { get; set; }
        //public DateTime BookingTime { get; set; }
        public string Venue { get; set; }
        public int NoOfTickets { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
