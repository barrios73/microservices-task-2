﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieBookingAPI.EntityModels
{
    public class MovieBookingDbContext: DbContext
    {
        public MovieBookingDbContext(DbContextOptions<MovieBookingDbContext> options) : base(options)
        {

        }


        public DbSet<Booking> Bookings { get; set; }
    }


}
