﻿using MovieBookingAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieBookingAPI.Utils
{
    public class Currency
    {

        public static decimal ConvertCurrency(string cur, InputCurrency input)
        {
            decimal amt=-1;
            //string curr = input.currency.ToUpper();

            switch (cur.ToUpper())
            {
                case "USD":
                    amt = input.amount * Rates.USDtoSGD;
                    return amt;
                case "AUD":
                    amt = input.amount * Rates.AUDtoSGD;
                    return amt;
                default:
                    return amt;
            }

          
        }

    }
}
